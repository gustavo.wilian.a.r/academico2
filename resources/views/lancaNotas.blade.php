@extends('template')
@section('conteudo')
    <h4> Lançamento de Notas </h4>
    @csrf
    <table class="table table-striped table-bordered">
        <tr>
            <th>Nome</th>
            <th>Turma</th>
            <th>nota 1</th>
            <th>nota 2</th>
            <th>nota 3</th>
        </tr>

        @foreach ($alunos as $aluno)
            <tr>  
                <td><a href="{{ url('listaDados/'.$aluno->id)}}"> {{ $aluno->nome }} </a> </td>
                <td>{{ $aluno->turma }}</td>   
                @php $aux_nota = false; @endphp
                @foreach ($notas as $nota)
                    @if ($aluno->id == $nota->id_aluno)
                        <td> <input type="number" class="notas" id="{{ $aluno->id }}:nota_1" value={{ $nota->nota_1 }} /> </td>   
                        <td> <input type="number" class="notas" id="{{ $aluno->id }}:nota_2" value={{ $nota->nota_2 }} /> </td> 
                        <td> <input type="number" class="notas" id="{{ $aluno->id }}:nota_3" value={{ $nota->nota_3 }} /> </td>   
                        @php $aux_nota = true; @endphp
                    @endif
                @endforeach
                @if (!$aux_nota)   
                    <td> <input type="number" class="notas" id="{{ $aluno->id }}:nota_1" value=' ' /> </td>   
                    <td> <input type="number" class="notas" id="{{ $aluno->id }}:nota_2" value=' ' /> </td>   
                    <td> <input type="number" class="notas" id="{{ $aluno->id }}:nota_3" value=' ' /> </td> 
                @endif
            </tr> 
        @endforeach
    </table>
    <br>
    <button type="button" id="salvar" class="btn btn-primary">Salvar dados</button>
@stop
@section('rodape')
@stop
@section('js')
<script type="text/javascript">
    jQuery(document).ready(function(){
        
        $("#salvar").click(function(){
            var _token = $("input[name='_token']").val();

            var inputs = $("input.notas");

            inputs.each(function(index, element){
                //console.log($(element).attr('id'));
                //console.log($(element).val());

                let id = $(element).attr('id');
                let nota = $(element).val();

                dados = id.split(':')
                console.log(dados);
                let nota_1 = 0;
                let nota_2 = 0;
                let nota_3 = 0;

                $.ajax({
                    url: '{{ route("salvaNotas")}}',
                    method: 'POST',
                    data: { _token: _token,
                        id_aluno : dados[0],
                        nota_1 : nota_1, 
                        nota_2 : nota_2, 
                        nota_3 : nota_3},
                    dataType: 'json',
                    success: function (data){
                        console.log(data);
                        if(data.ok === true){
                            $('.msg').removeClass('d-none').addClass('alert-success').html(data.msg)
                        } else {
                            $('.msg').removeClass('d-none').addClass('alert-danger').html(data.msg);
                        }
                    }
                });
            
            });
        });
    });
</script>
@stop
